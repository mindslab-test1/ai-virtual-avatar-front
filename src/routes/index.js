import Vue from 'vue';
import Router from 'vue-router';

import store from '../store/index';
import { RepositoryFactory } from '@/repositories/RepositoryFactory';
import adminRepository from "@/repositories/adminRepository";
const userRepository = RepositoryFactory.get('user');

Vue.use(Router);

const alreadyLoggedRouting = (to, from, next) => {
    const userString = localStorage.getItem('user');
    if(userString) {
        return next({'name' : 'MainHomeView'});
    }

    next();
};

const checkForUserAuth = async(to, from, next) => {
    let loggedIn = false;
    try {
        let {data} = await userRepository.me();
        store.commit('user/setUserInfo', data);
        loggedIn = true;
    }catch(err) {
        console.log(err)
     }

    if(loggedIn == false) {
        store.commit('user/clearUserInfo')
        alert('로그인이 필요한 기능입니다.')
        return next({'name' : 'UserSignInView'});
    }
    next()
};

const checkForAdminAuth = async(to, from, next) => {
    let authorized = false;
    try {
        let data = await adminRepository.checkAdminRole();
        authorized = true;
    } catch(err) {
        console.log(err)
    }

    if (authorized == false) {
        alert('관리자만 접속할 수 있는 페이지입니다.')
        return next({'name' : 'MainHomeView'});
    }
    next()
}


const routes = [
    {
        path: '/',
        // component: require('@/components/main/TestView').default,
        // meta: {
        //     title: 'TEST',
        // }
        redirect: '/users/sign_in'
    },   
    {
        path: '/Home',
        name: 'MainHomeView',
        beforeEnter: checkForUserAuth,
        component: require('@/components/main/MainHomeView').default,
        meta: {
            title: 'AVA 에디터',
        }
    },
    {
        path: '/download_list',
        name: 'DownloadListView',
        beforeEnter: checkForUserAuth,
        component: require('@/components/download/DownloadListView').default,
        meta: {
            title: 'AVA 다운로드 내역',
        }
    },
    {
        path: '/users/sign_up',
        name: 'UserSignUpView',
        component: require('@/components/users/UserSignUpView').default,
        meta: {
            title: 'AVA 회원가입',
        }
    },
    {
        path: '/policy/service',
        name: 'PolicyServiceView',
        component: require('@/components/policy/PolicyServiceView').default,
        meta: {
            title: 'AVA 이용약관',
        }
    },
    {
        path: '/policy/privacy',
        name: 'PolicyPrivacyView',
        component: require('@/components/policy/PolicyPrivacyView').default,
        meta: {
            title: 'AVA 개인정보 처리방침',
        }
    },
    {
        path: '/users/sign_in',
        name: 'UserSignInView',
        beforeEnter: alreadyLoggedRouting,
        component: require('@/components/users/UserSignInView').default,
        meta: {
            title: 'AVA 로그인',
        }
    },
    {
        path: '/payment',
        name: 'PaymentView',
        beforeEnter: checkForUserAuth,
        component: require('@/components/payment/PaymentView').default,
        meta: {
            title: 'AVA 결제페이지',
        }
    },
    {
        path: '/payment/management',
        name: 'PaymentManagementView',
        beforeEnter: checkForUserAuth,
        component: require('@/components/payment/PaymentManagementView').default,
        meta: {
            title: "AVA 결제정보"
        }
    },
    {
        path: '/admin/users',
        name: 'UserListView',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/manageusers/UserListView').default,
        meta: {
            title: "AVA 관리자 - 유저내역"
        }
    },
    {
        path: '/admin/users/:userNo',
        name: 'UserDetailView',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/manageusers/UserDetailView').default,
        props: true,
        meta: {
            title: "AVA 관리자 - 유저내역"
        }
    },
    {
        path: '/admin/statistics',
        name: 'StatisticsMain',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/statistics/Main').default,
        meta: {
            title: "AVA 관리자 - 통계"
        }
    },
    {
        path: '/admin/statistics/user',
        name: 'UserStatistics',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/statistics/User').default,
        meta: {
            title: "AVA 관리자 - 통계 - 유저 통계"
        }
    },
    {
        path: '/admin/statistics/voice',
        name: 'VoiceStatistics',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/statistics/Voice').default,
        meta: {
            title: "AVA 관리자 - 통계 - Voice Font 통계 - 목록"
        }
    },
    {
        path: '/admin/statistics/voice/:characterId',
        name: 'VoiceStatisticsDetail',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/statistics/VoiceDetail').default,
        meta: {
            title: "AVA 관리자 - 통계 - Voice Font 통계 - 상세"
        }
    },
    {
        path: '/admin/inquiries',
        name: 'ManageInquiryView',
        beforeEnter: checkForAdminAuth,
        component: require('@/components/admin/manageinquiry/InquiryManageView').default,
        meta: {
            title: "AVA 관리자 - 문의관리"
        }
    },
];
const router = new Router({
    //mode: 'history',
    routes: routes
});

const DEFAULT_TITLE = 'AVA Home';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;