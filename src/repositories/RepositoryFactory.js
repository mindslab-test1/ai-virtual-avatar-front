import CharacterRepository from './characterRepository';
import JobRepository from "./jobRepository";
import PlanRepository from './planRepository';
import UserRepository from './userRepository';
import DownloadRepository from './downloadRepository';
import PaymentRepository from './paymentRepository';
import ManageRepository from './manageRepository';
import AdminRepository from './adminRepository';
import CodeRepository from './codeRepository';

const repositories = {
  character: CharacterRepository,
  job: JobRepository,
  plan: PlanRepository,
  user: UserRepository,
  download: DownloadRepository,
  payment: PaymentRepository,
  manage: ManageRepository,
  admin: AdminRepository,
  code: CodeRepository,

  // other repositories ...
};

export const RepositoryFactory = {
  get: name => repositories[name]
};
  