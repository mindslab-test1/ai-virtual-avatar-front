import Repository from "./Repository";

const resource = "/code";

export default {
    getCodeDetailByGroupCode(groupCode) {
        return Repository.get(`${resource}/list/${groupCode}`);
    },
};