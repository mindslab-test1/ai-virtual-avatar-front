import Repository from "./Repository";

const resource = "/user";
export default {
    sign_in(authObj) {
        return Repository.post(`${resource}/sign_in`, authObj)
    },
    sign_out(authObj) {
        return Repository.post(`${resource}/sign_out`, authObj)
    },
    sign_up(paramObj) {
        return Repository.post(`${resource}/sign_up`, paramObj)
    },
    profanityCheck() {
        return Repository.get(`${resource}/profanity`)
    },
    setProfanityAgree() {
        return Repository.post(`${resource}/profanity`)
    },
    me() {
        return Repository.get(`${resource}/me`)
    }
}