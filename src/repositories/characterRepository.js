import Repository from "./Repository";

const resource = "/characters";
export default {
    list() {
        return Repository.get(`${resource}`);
    },
    dailyCharacter() {
        return Repository.get(`${resource}/dailyCharacter`);
    }
};