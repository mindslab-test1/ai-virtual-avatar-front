import Repository from "./Repository";

const resource = "/pay";

export default {

    getBillingFormInfo(planNo) {
        const params = {
            params: {
                planNo: planNo
            }
        }
        return Repository.get(`${resource}/billingForm`, params);
    },

    unsubscribeUser(userId) {
        return Repository.get(`${resource}/planCancel/${userId}`);
    },

    getPaymentFilterResponseInfo(data) {
        return Repository.post(`admin/filteringInfo`, data)
    },

    cancelPayment(payNo, userId) {
        const data = {
            payNo: payNo,
            userId: userId
        }
        return Repository.post('admin/payment/payCancel', data);
    },

    planCancel() {
        const params = {
            params: {}
        }
        return Repository.post(`${resource}/planCancel`, null, params);
    },

    getActiveBilling() {
        return Repository.get(`${resource}/getActiveBilling`);
    }

};