import Repository from "./Repository";

const resource = "/plan";

export default {
    getList() {
      return Repository.get(`${resource}/list`);
    },

    getUserActivePlanList(userNo) {
        return Repository.get(`${resource}/${userNo}/active`)
    }
};