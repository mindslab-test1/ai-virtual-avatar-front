import Repository from "./Repository";

const resource = "/download";
export default {
    download_list(pageNo) {
        return Repository.get(`${resource}?page=${pageNo}`)
    },
    request_download(paramObj) {
        return Repository.post(`${resource}`, paramObj)
    },
    check_request_download(paramObj) {
        return Repository.post(`${resource}/check`, paramObj)
    },
}