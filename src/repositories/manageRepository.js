import Repository from "./Repository";

const resource = "/manage";
export default {
    manage_user_list(pageNo,userNm,userId,userCellPhone,companyNm,planNo) {
        if (pageNo == null) {
            pageNo = '';
        }
        if (userNm == null) {
            userNm = '';
        }
        if (userId == null) {
            userId = '';
        }
        if (userCellPhone == null) {
            userCellPhone = '';
        }
        if (companyNm == null) {
            companyNm = '';
        }
        if (planNo == null) {
            planNo = '';
        }
        return Repository.get(`${resource}/users?page=${pageNo}&userNm=${userNm}&userId=${userId}&userCellPhone=${userCellPhone}&companyNm=${companyNm}&planNo=${planNo}`)
    },
}