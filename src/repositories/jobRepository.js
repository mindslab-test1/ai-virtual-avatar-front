import Repository from "./Repository";

const resource = "/job";
export default {
    myJobList() {
        return Repository.get(`${resource}/myJobList`);
    },
    getJobContent(jobId) {
        return Repository.get(`${resource}/${jobId}`);
    },
    getJobContentPreview(jobId) {
        return Repository.get(`${resource}/${jobId}/preview`);
    },
    saveJobContent(jobId, jobContent) {
        return Repository.put(`${resource}/${jobId}`, jobContent);
    },
    removeJobContent(jobId) {
        return Repository.post(`${resource}/${jobId}`);
    },
    updateJobScript(jobId, jobScriptId, jobScript) {
        return Repository.put(`${resource}/${jobId}/${jobScriptId}`, jobScript);
    },
    makeTTS(jobId, scriptId, characterId, text) {
        var params = new URLSearchParams();
        params.append("characterId", characterId);
        params.append('text', text);
        return Repository.post(`${resource}/makeTTS/${jobId}/${scriptId}`, params);
    }
};