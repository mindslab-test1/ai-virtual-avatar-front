import Repository from "./Repository";

const resource = "/admin";

export default {
    getUserInfo(userNo) {
        return Repository.get(`${resource}/user/${userNo}`);
    },
    getUserPayHistory(userNo) {
        return Repository.get(`${resource}/payment/${userNo}`);
    },
    getUserTimeline(userNo) {
        return Repository.get(`${resource}/userTimeline/${userNo}`);
    },

    checkAdminRole() {
        return Repository.get(`${resource}/authorize`);
    },

    // statistics
    getStatisticsMain(date) {
        return Repository.get(`${resource}/statistics/main/${date}`);
    },
    getUsertimelineList(param) {
        return Repository.get(`${resource}/statistics/user/timeline?${param}`);
    },
    getUsertimelineChart(param) {
        return Repository.get(`${resource}/statistics/user/timeline/chart?${param}`);
    },
    getVoiceFontStatisticsDataPage(param) {
        return Repository.get(`${resource}/statistics/voicefont?${param}`);
    },
    getVFCharacterData(characterId) {
        return Repository.get(`${resource}/statistics/voicefont/${characterId}`);
    },
    getVFCharacterDataList(characterId, param) {
        return Repository.get(`${resource}/statistics/voicefont/${characterId}/${param.year}`);
    },
    setVFCharacterData(characterId, param) {
        return Repository.post(`${resource}/statistics/voicefont/${characterId}`, {data : param});
    },

    getInquiryList(pageNo, inquiryNo, userNm, userId, inquiryType, inquiryStatus, answeredEmail) {
        if (pageNo == null) {
            pageNo = '';
        }
        if (inquiryNo == null) {
            inquiryNo = '';
        }
        if (userNm == null) {
            userNm = '';
        }
        if (userId == null) {
            userId = '';
        }
        if (inquiryType == null) {
            inquiryType = '';
        }
        if (inquiryStatus == null) {
            inquiryStatus = '';
        }
        if (answeredEmail == null) {
            answeredEmail = '';
        }
        return Repository.get(`${resource}/inquiry/list?page=${pageNo}&inquiryNo=${inquiryNo}&userNm=${userNm}&userId=${userId}&inquiryType=${inquiryType}&inquiryStatus=${inquiryStatus}&answeredEmail=${answeredEmail}`)
    },

    getInquiryDetail(inquiryNo) {
        return Repository.get(`${resource}/inquiry/detail/${inquiryNo}`);
    },

    replyInquiry(paramObj) {
        return Repository.post(`${resource}/inquiry/reply`, paramObj);
    }
};