export const closest = (el, predicate) => {
    do if (predicate(el)) return el;
    while (el = el && el.parentNode);
}

export const getElementAlias = (el) => {
    if (!el) return "null"
    return el.id || el.className || el.tagName;
}

export const hasClass = ( target, className ) => {
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}