import { HorizontalBar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: HorizontalBar,
    mixins: [reactiveProp],
    props: ['chartData', 'options'],
    mounted () {
        let plugin = function(chart) {
            let ctx = chart.ctx;
            ctx.restore();
            chart.data.datasets.forEach(function (dataset) {
                dataset.data.forEach((v, i) => {
                    const model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                    if(v !== 0){
                        ctx.fillText(Math.round(v)+'% ('+(model.datasetLabel[i].toLocaleString())+'자)', model.x+10, model.y+3);
                    }
                });
            });
            ctx.save();
        }
        this.addPlugin({
            id: 'showValue',
            beforeDraw: plugin
        })
        this.renderChart(this.chartData, this.options)
    }
}