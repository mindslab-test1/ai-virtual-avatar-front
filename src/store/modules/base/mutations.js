const clearModalStates = (state) => {
    state.modalStates = [];
};
const setModalStates = (state, targetKeys) => {
    state.modalStates = targetKeys || [];
};

const removeModalStates = (state, removeKeys) => {
    let modalStates = state.modalStates;
    removeKeys.forEach(targetKey => {
        let indexs = modalStates.map((modalState, idx) => modalState.name == targetKey ? idx : '').filter(String);
        for(let index in indexs) {
            modalStates.splice(index, 1)
        }
    });
    state.modalStates = modalStates;
};


export default {
    clearModalStates,
    setModalStates,
    removeModalStates,
};