
const isModal = ( state ) => {
    return function (vueObj) {
        return state.modalState[vueObj.$options.name];
    };
};

export default {
    isModal,
};