const setModal = ({ commit, state }, params ) => {
    let modalStates = []
    const isArray = Array.isArray(params)
    if(isArray || typeof params === 'object') {
        let objs = isArray ? params : [params]
        for(let idx in objs) {
            let obj = objs[idx] || {};
            if(typeof obj === 'string') {
                modalStates.push({name : obj, parameters : {}})
            }else {
                if(Object.keys(obj).indexOf('name') > -1) {
                    modalStates.push({name : obj['name'], parameters : obj['parameters']});
                }
            }
        }
    } else {
        modalStates.push({name: params, parameters : {}})
    }

    commit('setModalStates', modalStates)
};

const removeModal = ({ commit, state }, params ) => {
    let targetKeys = [];
    if(typeof(params) == 'object' && params._isVue) {
        targetKeys.push(params.$options.name);
    }else if((typeof(params) == 'object') && Array.isArray(params)) {
        for(let key in params) {
            targetKeys.push(key);
        }
    }else {
        targetKeys.push(params);
    }
    commit('removeModalStates', targetKeys)
};

const clearModal =  ({ commit, state }) => {
    commit('clearModalStates')
};

export default {
    setModal,
    removeModal,
    clearModal,
};