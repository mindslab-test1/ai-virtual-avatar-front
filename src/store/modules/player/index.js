import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    cursor: -1,
    changeTrack: { cursor: -1, needForConvert: false, endPlay: false },
    forceTrackStop: false,
    isPlaying: false
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};