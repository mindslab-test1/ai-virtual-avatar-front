const updateCursor = (state, cursor) => {
    state.cursor = cursor; 
};

const clearCursor = (state) => {
    state.cursor = -1;
};

const forceChangeTrack = (state, {cursor, needForConvert, endPlay}) => {
    state.changeTrack = { cursor, needForConvert, endPlay };
};

const clearChangeTrack = (state) => {
    state.changeTrack = { cursor: -1, needForConvert: false, endPlay: false };
};

const setForceTrackStop = (state, flag) => {
    state.forceTrackStop = flag;
}

const setIsPlaying = (state, flag) =>{
    state.isPlaying = flag;
}

export default {
    updateCursor, 
    clearCursor,
    forceChangeTrack,
    clearChangeTrack,
    setForceTrackStop,
    setIsPlaying,

};