import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    characterMap: {},
    characters: [],
    last_character: {},
    last_character_uuid: ''
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};