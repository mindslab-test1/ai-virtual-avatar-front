const getCharacter = (state) => (characterId) => {
    return state.characters.find(character => character.characterId === characterId);
};
export default {
    getCharacter,
};