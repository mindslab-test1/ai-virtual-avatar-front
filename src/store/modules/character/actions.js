/**
 * Characters List 저장 - Action calle
 * @param {*} characters 
 */
const setCharacters = ({ commit }, characters) => {
    return commit('setCharacters', characters);
};

const setLastCharacter = ({ commit }, character) => {
    commit('setLastCharacter', character)
};

const showModal = ({ commit }, key) => {
    let data = { };
    data[key] = true;
    commit('showModal', data)
};

export default {
    setCharacters,
    setLastCharacter,
    showModal,
};