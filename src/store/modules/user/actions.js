import 'url-search-params-polyfill';

import { RepositoryFactory } from '@/repositories/RepositoryFactory';
const userRepository = RepositoryFactory.get('user');

const signIn =  ({ commit }, loginObj) => { 
    var params = new URLSearchParams();
    params.append('userId', loginObj.userId);
    params.append('userPw', loginObj.userPw);
    return new Promise((resolve, reject) => {
        userRepository.sign_in(params).then((res) => {
            if(res.status == 200) {
                let userInfo = res.data.userInfo
                let {access_token, refresh_token} = res.data.tokenInfo;
                commit('setUserToken', {access_token, refresh_token });
                commit('setUserInfo', userInfo)
                return resolve();
            }else if(res.status == 401) {
                return reject(res.data.errorKorMessage);
            }else {
                return reject("로그인 실패(500:시스템 오류 발생)");
            }
        }).catch((error) => {
            return reject("로그인 실패(" + error.response.status + ")");
        })
    });
};

const signOut =  ({ commit, state, getters }) => {

    let userNo = state.userInfo.userNo;
    let refresh_token = getters.getRefreshTokenFromLocalStorage();
    
    return new Promise((resolve, reject) => {
        if(!refresh_token) {
            commit('clearUserInfo')
            return resolve("로그아웃 처리 되었습니다.");
        }else {
            let params = { 'userNo': userNo, 'refresh_token' : refresh_token };
            userRepository.sign_out(params).then((res) => {
                commit('clearUserInfo')
                return resolve(res.data.processMessage);
            }).catch((error) => {
                return reject("로그아웃 실패(" + error.response.status + ")");
            });
        }
    });
};

const signUp =  ({ commit }, signUpObj) => {
    return new Promise((resolve, reject) => {
        userRepository.sign_up(signUpObj).then((res) => {
            const data = res.data;
            if(data.processStatus == "SIGNUP_DUPLICATE") {
                return reject(data.processMessage)
            }
            commit('clearUserInfo')
            return resolve(data.processMessage);
        }).catch((error) => {
            return reject("회원가입 실패(" + error.response.status + ")");
        });
    });
};

const profanityAgree =  ({ commit }) => {
    return new Promise((resolve, reject) => {
        userRepository.setProfanityAgree().then((res) => {
            const data = res.data;
            if(data.profanityStatus == "FAIL") {
                return reject(data.message);
            }
            commit('setProfanityAgree', true);
            return resolve(data.message);
        }).catch((error) => {
            return reject("욕설사용 동의 정보등록 실패(" + error.response.status + ")");
        });
    });
};

const setProfanityAgree =  ({ commit }, agree) => {
    commit('setProfanityAgree', agree);
};

export default {
    signIn,
    signOut,
    signUp,
    setProfanityAgree,
    profanityAgree,
};