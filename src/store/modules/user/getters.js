const getUserAgrees = (state) => () => {
    if(state.userInfo) {
        return state.userInfo.userAgrees;
    }
    return {};
};

// JOB Script getter(index)
const getRefreshTokenFromLocalStorage = () => ()  =>{
    return localStorage.getItem("refresh_token") || '';
};

const getProfanityAgreed = (state) => () => {
    return state.profanityAgree;
};

export default {
    getRefreshTokenFromLocalStorage,
    getProfanityAgreed,
    getUserAgrees,
};