import uuidv4 from 'uuid/v4';

import { RepositoryFactory } from '@/repositories/RepositoryFactory';
const JobRepository = RepositoryFactory.get('job');

/**
 * JOB 기본정보 세팅(jobId, jobTitle)
 * @param {*} jobId 
 * @param {*} jobTitle 
 */
const setJobBasicInfo = ({commit}, {jobId, jobTitle}) => {
    commit('setJobId', jobId);
    commit('setJobTitle', jobTitle);
};

/**
 * 신규 JOB 생성
 * - views/edit/EditJobButton.vue 사용
 * - views/MakeHomeView.vue 사용
 */
const clearJobBasicInfo = ({commit}) => {
    commit('setJobId', uuidv4());
    commit('setJobTitle', '');
};

const insertAtJobScript = ({commit, rootState}, {index, characterId, text, fake=false}) => {
    let addJobScript = { 'scriptId' : uuidv4(), 'scriptText' : (text ? text : ''), 'settings' : {}, 'ttsState' : 'W', 'rowState' : 'A' };
    if(!characterId) {
        characterId = rootState.character.last_character.characterId;
    }

    addJobScript = Object.assign(addJobScript, {characterId, fake});
    commit('insertAtJobScript', { index: index, script: addJobScript });
};

/**
 * 스크립트 삭제 처리 - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} index 삭제하고자하는 index
 */
const removeAtJobScript = ( { commit, getters }, index) => {
    if (getters.isChangeCandidate(index)) {
        commit('pushDeletedJobScript', {index});
    }
    commit('removeAtJobScript', {index});
};

/**
 * 변경 텍스트 업데이트 처리 - Action callee
 * @param {*} index 변경될 인덱스
 * @param {*} scriptText 변경될 텍스트
 */
const updateAtJobScriptText = ({ commit, state, getters }, {index, text }) => {
    let changeCandidate = getters.isChangeCandidate(index);
    let hasTextLength = text.length;
    commit('updateAtJobScriptText', { 'index' : index, 'scriptText' : text, 'rowState': changeCandidate ? 'U' : 'A' })
    commit('updateAtJobScriptTTSResult', {index});
    return state.jobScripts[index];
};


/**
 * 스크립트 재할당 처리 - Action callee
 * @param {*} targetScripts 
 * @param {*} characterId 
 */
const setReallocationScripts = ({commit}, {targetScriptIndexs, characterId}) => {
    if(Array.isArray(targetScriptIndexs)) {
        targetScriptIndexs.forEach((index) => {
            commit('updateAtJobScriptFromPayLoad', { 'index' : index, 'payload' : {'characterId' : characterId }})
            commit('updateAtJobScriptTTSResult', {index});
        });
    }else {
        let index = targetScriptIndexs;
        commit('updateAtJobScriptFromPayLoad', { 'index' : index, 'payload' : {'characterId' : characterId }})
        commit('updateAtJobScriptTTSResult', {index});
    }
};

/**
 * JOB Script All Row 삭제 처리 - Action calle
 */
const clearJobScripts = ({commit}) => {
    return commit('setJobScripts', []);
};


/**
 * JOB Script List 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobScripts 
 */
const setJobScripts = ({ commit }, jobScripts) => {
    return commit('setJobScripts', jobScripts);
};



/**
 * TTS 변환 대상 감지 처리
 * - views/player/Player.vue 사용
 * @param {*} index 인덱스
 */
const detectMoreConvertScripts = async ({state, rootState, commit, dispatch, getters, rootGetters}, index) => {
    const divideScripts = await dispatch("divideByIndexOfScripts", index);
    const targetScripts = await dispatch("detectConvertScriptsFromArray", {scripts: divideScripts});
    console.log(divideScripts)
    console.log(targetScripts)

    let currentScript = targetScripts.detectRangeScripts[0];
    if (currentScript.ttsState !== 'S') {
        const syncConvertTargetScripts = targetScripts.convertWaitScripts.splice(0, 1);
        currentScript = Promise.all(syncConvertTargetScripts.map( script => {
            return dispatch("requestMakeTTSLoop", {jobId: state.jobId, jobScript: script}).then(script=> {
                JobRepository.updateJobScript(state.jobId, script.scriptId, script)
                return script;
            })
        }));
    }

    const convertWaitScripts = targetScripts.convertWaitScripts;
    if (convertWaitScripts.length > 0) {
        // console.log('convertWaitScripts convert 대상!!! convertWaitScripts.length = ' +  convertWaitScripts.length);
        convertWaitScripts.forEach(script => {
            dispatch("requestMakeTTSLoop", {jobId: state.jobId, jobScript: script});
        });
    }

    // await JobScriptProcessor.detectMoreConvertScripts(commit, state.jobScripts, index);
    // const jobScript = state.jobScripts[index];
    // await JobRepository.updateJobScript(state.jobId, jobScript.scriptId, jobScript);
};

//const divideByIndexOfScripts = ({state, rootState, commit, dispatch, getters, rootGetters}, index) => {
const divideByIndexOfScripts = ({state}, index) => {
    return new Promise ((resolve) => {
        let jobScripts = state.jobScripts;

        return resolve ({
            'prev' : jobScripts.slice(0, index),
            'next' : jobScripts.slice(index, jobScripts.length)
        });
    });
};

const detectConvertScriptsFromArray = ({}, {scripts, options = {}}) => {
    return new Promise ((resolve, reject) => {
        const range = options.range || 'next';
        const detectScriptsSize = options.detectScriptsSize || 5;
        const detectActUpSize = options.detectActUpSize || 4;

        const targetScriptsAll = scripts[range];
        const detectRangeScripts = targetScriptsAll.slice(0, detectScriptsSize);

        if(!String.prototype.toDate) {
            String.prototype.toDate = function() {
                const [year, month, day] = this.split("-")
                return new Date(year, month - 1, day)
            }
        }
        if(!Date.prototype.addDays) {
            Date.prototype.addDays = function(days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            }
        }

        // 7일 경과 TTS 변환 대상
        let nowDate = new Date();
        detectRangeScripts.forEach((script, index) => {
            if(script.hasOwnProperty("ttsExpiredDate") && !!script.ttsExpiredDate ) {
                if(script.ttsExpiredDate.toDate().addDays(6) <= nowDate) {
                    script.ttsState = "W";
                }
            }
        });

        const waitScriptSates = ['W', 'E']; // WAIT, ERROR
        
        const stateFilter = (scripts, ...compareStateArgs) => { return scripts.filter(script => compareStateArgs.indexOf(script.ttsState) > -1); };
        const detectRangeCompleteScripts = stateFilter(detectRangeScripts, 'S'); //STANDBY
        const detectRangeWaitScripts = stateFilter(detectRangeScripts.slice(0, detectActUpSize), ...waitScriptSates);
        
        let convertWaitScripts = [];
        //console.log("detectRangeCompleteScripts.length", detectRangeCompleteScripts.length);
        //console.log("detectRangeWaitScripts.length", detectRangeWaitScripts.length);

        if (detectRangeCompleteScripts.length <= detectActUpSize) {
            convertWaitScripts = stateFilter(targetScriptsAll, ...waitScriptSates).slice(0, detectScriptsSize);
        }else if((detectRangeCompleteScripts.length >=  detectActUpSize) && (detectRangeWaitScripts.length > 0)) {
            convertWaitScripts = detectRangeWaitScripts;
        }
        return resolve({ detectRangeScripts : detectRangeScripts,  convertWaitScripts : convertWaitScripts });
    });
};

const requestMakeTTSLoop = async ({state, commit, getters}, {jobId, jobScript}) => {
    let reTry = 1;
    const reTryMax = 5;
    const {scriptId, scriptText, characterId} = jobScript;
    let index = getters.getJobScriptIndexFindByScriptId(scriptId);
    if(scriptText.length === 0) {
        return new Promise((resolve, reject) => {
            return reject(state.jobScripts[index])
        });
    }
    const sleep = (ms) => { return new Promise(resolve => setTimeout(resolve, ms)); };
    let updateTTSReponse = { 'index': index, 'ttsState' : 'C' };
    commit('updateAtJobScriptTTSResult', updateTTSReponse);

    let result;
    while (reTry <= reTryMax ) {
        try {
            let {status, data} = await JobRepository.makeTTS(jobId, scriptId, characterId, scriptText)
            if (status == 200) {
                   result = {  'ttsState' : 'S', 'ttsResource' : data.resource, 'ttsPlayTime': data.resourceDuration, 'ttsExpiredDate': data.expiryDate }
            }else {
                result = { 'ttsState' : 'E' };
            }
            console.log(result)
        } catch(e) {
            result = { 'ttsState' : 'E' };
        };
        
        if(result.ttsState == 'S') {
            break;
        } else {
            if (reTry >= reTryMax) {
                console.log('%c reTry 호출 초과 :' + reTryMax , 'background: yellow; color: red');
            } else {
                console.log('%c reTry 호출 시도 :' + reTry , 'background: red; color: black');
            }
        }
        reTry++;
        await sleep(2000);
    }

    updateTTSReponse = Object.assign({ 'index': index }, result);
    commit('updateAtJobScriptTTSResult', updateTTSReponse);

    return new Promise((resolve, reject) => {
        return resolve(state.jobScripts[index])
    });
    
};




/**
 * JOB List 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobList 작업 리스트
 */
const setJobList = ({ commit }, jobList) => {
    return commit('setJobList', jobList);
};

/**
 * JOB Title 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobTitle 작업 제목명
 */
const setJobTitle = ({commit}, jobTitle) => {
    return commit('setJobTitle', jobTitle);
};

/**
 * @param {*} clearJobTitle 
 */
const clearJobTitle = ({commit}) => {
    return commit('setJobTitle', '');
};

/**
 * JOB Content 저장(jobId, jobTitle, jobSpeakers, jobScripts);
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobId 
 * @param {*} jobTitle 
 * @param {*} jobScripts 
 */
const setJobContent = ({commit}, {jobId, jobTitle, jobScripts}) => {
    commit('setJobId', jobId);
    commit('setJobTitle', jobTitle);
    commit('setJobScripts', jobScripts);
};
/**
 *  JOB Content 초기화(jobId, jobTitle, jobScripts);
 * - views/edit/EditJobButton.vue 사용
 */
const clearJobContent = ({commit}) => {
    commit('setJobId', uuidv4());
    commit('setJobTitle', '');
    return commit('setJobScripts', []);
};


/**
 * JOB Script 처리 / END
 **/

export default {
    setJobBasicInfo,
    clearJobBasicInfo,

    insertAtJobScript,
    removeAtJobScript,
    updateAtJobScriptText,

    // JOB Script 처리 / START
    clearJobScripts,
    setJobScripts,

    //Detect Convert Script
    detectMoreConvertScripts,
    divideByIndexOfScripts,
    detectConvertScriptsFromArray,
    requestMakeTTSLoop,
    
    
    setJobList,
    setJobTitle,
    clearJobTitle,
    setJobContent,
    clearJobContent,

    setReallocationScripts,
    // JOB Script 처리 / END
};