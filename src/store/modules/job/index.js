import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    jobList: [],
    jobId: "",
    jobTitle: "",
    
    jobScripts: [],
    deletedJobScripts: [],

};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};