const setBillingFormData = ({commit}, data)=>{
    commit('setBillingFormData', data);
}

export default {
    setBillingFormData
};