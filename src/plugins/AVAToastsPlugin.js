import AVAToasts from '@/components/common/AVAToasts';
import { EventBus } from '@/utils/bus';

const ToastsPlugin = {
  install(Vue) {
    Vue.prototype.$toast = {
      send(message, type, options) {
        EventBus.$emit("toast-message", {
          message: message,
          type: type,
          options: options
        });
      },

      success(message, options = {}) {
        this.send(message, "success", options);
      },

      warning(message, options = {}) {
        this.send(message, "warning", options);
      },

      info(message, options = {}) {
        this.send(message, "information", options);
      },

      error(message, options = {}) {
        this.send(message, "danger", options);
      }
    };

    Vue.component("AVAToasts", AVAToasts);
  }
};

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(ToastsPlugin);
}

export default ToastsPlugin;
