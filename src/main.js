import Vue from 'vue';

import App from './App';
import router from './routes';
import store from './store';
import filter from "@/utils/filters";
import VueGtag from "vue-gtag";

import AVAToastsPlugin from '@/plugins/AVAToastsPlugin.js';

Vue.use(AVAToastsPlugin);

Vue.use(VueGtag, {
    config: { id: "G-VXNJ98B2PW" },
    pageTrackerTemplate(to) {
        return {
            page_title: to.meta.title,
            page_path: to.path
        }
    },
}, router);

Vue.config.productionTip = false;
new Vue({
    filter,
    router,
    store,
    el: '#app',
    render: h => h(App),
});